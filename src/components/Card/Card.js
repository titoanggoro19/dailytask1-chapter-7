import React from "react";
import Header from "../Header/Header";
import Button from "../Button/Button";

const Card = (props) => {
  const { title, description, imgSrc, imgAlt, btnText, btnHref } = props;

  return (
    <div className="row justify-content-center">
      <div className="card me-2 mb-2" style={{ width: "18rem" }}>
        <Header title="Card 1" />
        <img src={imgSrc} className="card-img-top" alt={imgAlt} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <Button variant="basic">{btnText}</Button>
        </div>
      </div>
      <div className="card me-2 mb-2" style={{ width: "18rem" }}>
        <Header title="Card 2" />
        <img src={imgSrc} className="card-img-top" alt={imgAlt} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <Button variant="basic">{btnText}</Button>
        </div>
      </div>
      <div className="card me-2 mb-2" style={{ width: "18rem" }}>
        <Header title="Card 3" />
        <img src={imgSrc} className="card-img-top" alt={imgAlt} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <Button variant="basic">{btnText}</Button>
        </div>
      </div>
      <div className="card me-2 mb-2" style={{ width: "18rem" }}>
        <Header title="Card 4" />
        <img src={imgSrc} className="card-img-top" alt={imgAlt} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <Button variant="basic">{btnText}</Button>
        </div>
      </div>
    </div>
  );
};

export default Card;
