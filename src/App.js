import Card from "./components/Card/Card";
import Header from "./components/Header/Header";
import HeadingTitle from "./components/HeadingTitle/HeadingTitle";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Header title="Hello FSW13" />
      <HeadingTitle title="Selamat Datang" />

      <Card
        title="Hello"
        description="Lorem ipsum dolor sit amet"
        btnText="Go Somewhere"
        btnHref="https://google.com"
        imgSrc="https://placeimg.com/320/240/any"
        imgAlt="Hello"
      />
    </div>
  );
}

export default App;
